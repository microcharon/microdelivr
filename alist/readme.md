## Welcome to Microcharon Download Center

🛖 **官方破站**

- [Microcharon - The Way of Developer](https://www.microcharon.top/)

🔗 **旗下開放站點**

- [Microcharon Download Center](https://drive.microcharon.top/)
- [Microcharon OneDrive Cloud](https://cloud.microcharon.top/)

🌐 **WebDav 配置參數**

- 連結 | URL: https://drive.microcharon.top/dav/
- 主機 | Host: drive.microcharon.top
- 路徑 | Path: /dav/
- 協議 | HTTPS: SSL
- 埠口 | Port: 443
- 賬號 | User: guest
- 密碼 | Password: guest

🐾 **免責聲明**

本站點符合 DMCA 數位權法，若侵犯了您的著作權，即您的受版權保護的材料已發佈在本站（[Microcharon Download Center](https://drive.microcharon.top/)）上，或者通過搜索引擎返回指向您受版權保護材料的超鏈接，如果您希望刪除此材料，則必須提供書面通信，詳細信息如下

- 相關內容的完整名稱
- 發布指向詳細信息帖子的鏈接。
- 您的公司名稱
- 個人信息
- 電話號碼

通過電子郵件發送侵權通知（我們不會忽略電子郵件）：admin#microcharon.top 或者 hubcharon#gmail.com，移除請求將在 1 小時至 48 小時內處理

<a href="https://t.me/microcharon"><img alt="Contact me" src="https://img.shields.io/badge/Contact-Telegram-blue?style=flat&logo=telegram"></a> <a href="https://buyvm.net"><img alt="Hosted by BuyVM" src="https://img.shields.io/badge/Hosted-BuyVM-lightgray?style=flat&logo=debian"></a> <a href="https://www.cloudflare.com"><img alt="Protected" src="https://img.shields.io/badge/Protected-Cloudflare-orange?style=flat&logo=cloudflare"></a> <a href="https://www.paypal.com/paypalme/paycharon"><img alt="Sponsor" src="https://img.shields.io/badge/Sponsor-PayPal-blue?style=flat&logo=paypal"></a>